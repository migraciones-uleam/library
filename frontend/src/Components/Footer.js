import React from 'react'
import './Footer.css'

import TwitterIcon from '@material-ui/icons/Twitter';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import TelegramIcon from '@material-ui/icons/Telegram';
import InstagramIcon from '@material-ui/icons/Instagram';

function Footer() {
    return (
        <div className='footer'>
            <div>
                <div className='footer-data'>
                    <div className="contact-details">
                        <h1>Contactanos</h1>
                        <p>ULEAM</p>
                        <p>Manta, Ecuador</p>
                        <p>Calle Universitaria</p>
                        <p><b>Correo:</b>librarian@uleam.com</p>
                    </div>
                    <div className='usefull-links'>
                        <h1>Otros enlaces</h1>
                        <a href='#home'>Enlace 1</a>
                        <a href='#home'>Enlace 2</a>
                        <a href='#home'>Enlace 3</a>
                        <a href='#home'>Enlace 4</a>
                    </div>
                    <div className='librarian-details'>
                        <h1>Librarian</h1>
                        <p>ULEAM</p>
                        <p>Educación</p>
                        <p>Contact: +593 0999999999</p>
                    </div>
                </div>
                <div className="contact-social" >
                    <a href='#home' className='social-icon'><TwitterIcon style={{ fontSize: 40,color:"rgb(283,83,75)"}} /></a>
                    <a href='#home' className='social-icon'><LinkedInIcon style={{ fontSize: 40,color:"rgb(283,83,75)"}} /></a>
                    <a href='#home' className='social-icon'><TelegramIcon style={{ fontSize: 40,color:"rgb(283,83,75)"}} /></a>
                    <a href='#home' className='social-icon'><InstagramIcon style={{ fontSize: 40,color:"rgb(283,83,75)"}} /></a>
                </div>
            </div>
            <div className='copyright-details'>
                <p className='footer-copyright'>&#169; 2023 Todos los derechos reservados<br /></p>
            </div>
        </div>
    )
}

export default Footer