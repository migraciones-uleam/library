import React from 'react'
import './WelcomeBox.css'

function WelcomeBox() {
    return (
        <div className='welcome-box'>
            <p className='welcome-title'>Bienvenido a la biblioteca</p>
            <p className='welcome-message'>Alimenta tu mente con conocimiento<br/>
            <span className='welcome-submessage'>Toma un libro y comienza a leer</span></p>
        </div>
    )
}

export default WelcomeBox
